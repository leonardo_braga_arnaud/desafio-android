package br.eti.arnaud.desafioandroid.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import android.os.Handler;
import android.os.Parcelable;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.eti.arnaud.desafioandroid.MainActivity;
import br.eti.arnaud.desafioandroid.R;
import br.eti.arnaud.desafioandroid.classes.DribbbleRecyclerViewAdapter;
import br.eti.arnaud.desafioandroid.classes.EndlessRecyclerOnScrollListener;
import br.eti.arnaud.desafioandroid.classes.OnFinishPageDownload;
import br.eti.arnaud.desafioandroid.classes.RemoteDataCache;
import br.eti.arnaud.desafioandroid.classes.SwipeableRecyclerViewTouchListener;
import br.eti.arnaud.desafioandroid.classes.UndoBarController;
import br.eti.arnaud.desafioandroid.models.dribbble.Page;
import br.eti.arnaud.desafioandroid.models.dribbble.Shot;
import jp.wasabeef.recyclerview.animators.adapters.AnimationAdapter;
import jp.wasabeef.recyclerview.animators.adapters.SlideInBottomAnimationAdapter;

/*
 * Este fragment contem a resposta para o desafio proposto.
 */
public class HomeFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RemoteDataCache mRemoteDataCache;
    private View mFragmentView;
    private Context mContext;
    private DribbbleRecyclerViewAdapter mDribbbleRecyclerViewAdapter;
    private AnimationAdapter mAnimationAdapter;
    private View mProgressBar;
    private UndoBarController mUndoBarController;


    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mFragmentView = getView();
        mContext = getActivity();
        mProgressBar = mFragmentView.findViewById(R.id.pb_loading_list);

        configRecyclerView();
        configUndobarController();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mRemoteDataCache = (RemoteDataCache) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement RemoteDataCache");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mRemoteDataCache = null;
    }

    private void configRecyclerView(){
        if (mFragmentView != null){
            mRecyclerView = (RecyclerView) mFragmentView.findViewById(R.id.shots_recycler_view);
            final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(mContext);
            mRecyclerView.setLayoutManager(linearLayoutManager);

            ArrayList<Page> dribbblePages = mRemoteDataCache.getDribbblePages();
            if (dribbblePages.isEmpty()){
                progressBarVisibility(View.VISIBLE);
                mRemoteDataCache.downloadPage(1, new OnFinishPageDownload() {
                    @Override
                    public void onFinish(ArrayList<Page> dribbblePages) {
                        progressBarVisibility(View.GONE);
                        showRecyclerView(linearLayoutManager, dribbblePages);
                    }
                });
            } else {
                showRecyclerView(linearLayoutManager, dribbblePages);
            }
        }
    }

    private void configEndlessScrollListener(LinearLayoutManager linearLayoutManager
            , ArrayList<Page> dribbblePages){
        boolean isConnected = ((MainActivity)mContext).checkInternetConnection();
        if (isConnected){
            mRecyclerView.setOnScrollListener(
                    new EndlessRecyclerOnScrollListener(linearLayoutManager, dribbblePages.size()) {
                        @Override
                        public void onLoadMore(final int currentPage) {
                            progressBarVisibility(View.VISIBLE);

                                mRemoteDataCache.downloadPage(currentPage, new OnFinishPageDownload() {
                                    @Override
                                    public void onFinish(final ArrayList<Page> dribbblePages) {
                                        mDribbbleRecyclerViewAdapter.addAllShotsOfPage(
                                                dribbblePages.get(currentPage - 1));
                                    }
                                });

                        }

                        @Override
                        public void onLoadFinish() {
                            progressBarVisibility(View.GONE);
                        }
                    });
        } else {
            Toast.makeText(mContext, R.string.connection_message
                    , Toast.LENGTH_LONG).show();
        }

    }

    private void configScrollAnimation(ArrayList<Page> dribbblePages){
        mDribbbleRecyclerViewAdapter = new DribbbleRecyclerViewAdapter(mContext, dribbblePages);
        mAnimationAdapter =
                new SlideInBottomAnimationAdapter(mDribbbleRecyclerViewAdapter);
        mRecyclerView.setAdapter(mAnimationAdapter);
    }

    private void configSwipeableRecyclerView(final ArrayList<Page> dribbblePages){
        SwipeableRecyclerViewTouchListener swipeTouchListener =
                new SwipeableRecyclerViewTouchListener(mRecyclerView,
                        new SwipeableRecyclerViewTouchListener.SwipeListener() {
                            @Override
                            public boolean canSwipe(int position) {
                                return true;
                            }

                            @Override
                            public void onDismissedBySwipeLeft(RecyclerView recyclerView
                                    , int[] reverseSortedPositions) {
                                remove(reverseSortedPositions);
                            }

                            @Override
                            public void onDismissedBySwipeRight(RecyclerView recyclerView
                                    , int[] reverseSortedPositions) {
                                remove(reverseSortedPositions);
                            }

                            private void remove(int[] reverseSortedPositions){
                                mUndoBarController.hideUndoBar(true);
                                for (int position : reverseSortedPositions) {
                                    List<Shot> allLoadedShots = mDribbbleRecyclerViewAdapter
                                            .getAllLoadedShots();
                                    Shot removedShot = allLoadedShots.get(position);
                                    mRemoteDataCache.addToRemoveStack(removedShot);
                                    allLoadedShots.remove(position);
                                    mAnimationAdapter.notifyItemRemoved(position);
                                    mAnimationAdapter.notifyItemRangeChanged(position,
                                            mDribbbleRecyclerViewAdapter.getItemCount());
                                    mDribbbleRecyclerViewAdapter.setLastRemovedShot(removedShot
                                            , position);

                                }

                                mAnimationAdapter.notifyDataSetChanged();
                                mUndoBarController.showUndoBar(false
                                        , getString(R.string.shot_removed), null);
                            }
                        });

        mRecyclerView.addOnItemTouchListener(swipeTouchListener);

    }

    private void showRecyclerView(LinearLayoutManager linearLayoutManager
            , ArrayList<Page> dribbblePages){
        configSwipeableRecyclerView(dribbblePages);
        configEndlessScrollListener(linearLayoutManager, dribbblePages);
        configScrollAnimation(dribbblePages);

    }

    private void progressBarVisibility(int visibility){
        mProgressBar.setVisibility(visibility);
    }

    private void configUndobarController(){
        mUndoBarController = new UndoBarController(mFragmentView.findViewById(R.id.undobar)
                , new UndoBarController.UndoListener() {
            @Override
            public void onUndo(Parcelable token) {
                mDribbbleRecyclerViewAdapter.recoverLastRemovedShot(mAnimationAdapter);
                mRemoteDataCache.removeLastFromRemoveStack();
            }

            @Override
            public void onUndoDisappears() {}
        });
    }



    @Override
    public void onDestroy() {
        try {
            mUndoBarController.hideUndoBar(true);
        } catch (NullPointerException e){
            e.printStackTrace();
        }
        super.onDestroy();
    }
}
