package br.eti.arnaud.desafioandroid.classes;

import java.util.ArrayList;

import br.eti.arnaud.desafioandroid.models.dribbble.Page;

/**
 * Created by Leonardo on 22/03/2015.
 */
public interface OnFinishPageDownload {
    public void onFinish(ArrayList<Page> dribbblePages);
}
