package br.eti.arnaud.desafioandroid.classes;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Adaptado por Leonardo B. Arnaud em 22/03/2015.
 *
 * Referências:
 * @link https://gist.github.com/ssinss/e06f12ef66c51252563e/download#
 * @link http://stackoverflow.com/a/26561717/500714
 * @link https://github.com/codepath/android_guides/wiki/Endless-Scrolling-with-AdapterViews
 *
 * Possibilita identificar o final da listagem e disparar aviso para ler mais dados da lista.
 *
 */
public abstract class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {

    // Total de itens no dataset após o último load
    private int previousTotal = 0;

    // True se estiver esperando o dataset ser lido
    private boolean loading = true;

    // O mínimo de itens restantes abaixo da sua posiçao atual antes de ler mais.
    private int visibleThreshold = 5;

    int firstVisibleItem;
    int visibleItemCount;
    int totalItemCount;

    private int currentPage;

    private LinearLayoutManager mLinearLayoutManager;

    public EndlessRecyclerOnScrollListener(LinearLayoutManager linearLayoutManager, int currentPage) {
        this.mLinearLayoutManager = linearLayoutManager;
        this.currentPage = currentPage;
    }

    public void setCurrentPage(int currentPage){
        this.currentPage = currentPage;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = mLinearLayoutManager.getItemCount();
        firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false;
                previousTotal = totalItemCount;
                onLoadFinish();
            }
        }
        if (!loading && (totalItemCount - visibleItemCount)
                <= (firstVisibleItem + visibleThreshold)) {
            currentPage++;
            onLoadMore(currentPage);
            loading = true;
        }
    }

    public abstract void onLoadMore(int currentPage);
    public abstract void onLoadFinish();
}