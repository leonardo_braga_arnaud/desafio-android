package br.eti.arnaud.desafioandroid.classes;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.List;

import br.eti.arnaud.desafioandroid.MainActivity;
import br.eti.arnaud.desafioandroid.R;
import br.eti.arnaud.desafioandroid.fragments.ShotDetailFragment;
import br.eti.arnaud.desafioandroid.models.dribbble.Page;
import br.eti.arnaud.desafioandroid.models.dribbble.Shot;
import jp.wasabeef.recyclerview.animators.adapters.AnimationAdapter;

/**
 * Created by Leonardo on 22/03/2015.
 */
public class DribbbleRecyclerViewAdapter extends
        RecyclerView.Adapter<DribbbleRecyclerViewAdapter.ViewHolder> {

    private Context mContext;
    private List<Shot> mShots;
    private Shot lastRemovedShot;
    private int lastRemovedPosition;

    public DribbbleRecyclerViewAdapter(Context context, List<Page> dribbblePages) {
        this.mContext = context;
        mShots = new ArrayList<>();
        for (Page page : dribbblePages){
            mShots.addAll(page.shots);
        }
    }

    @Override
    public int getItemCount() {
        return mShots.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        Shot shot = mShots.get(position);
        viewHolder.shotTitle.setText(shot.title);

        Ion.with(viewHolder.shotImage)
                .placeholder(R.mipmap.no_image)
                .error(R.mipmap.no_image)
                .load(shot.image_url);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.dribbble_card_item, viewGroup, false);

        return new ViewHolder(itemView);
    }

    public void addAllShotsOfPage(Page page){
        int insertPosition = getItemCount();
        mShots.addAll(page.shots);
        notifyItemInserted(insertPosition);
        notifyItemRangeChanged(insertPosition, getItemCount());
    }

    public List<Shot> getAllLoadedShots(){
        return mShots;
    }

    public void setLastRemovedShot(Shot lastRemovedShot, int lastRemovedPosition){
        this.lastRemovedShot = lastRemovedShot;
        this.lastRemovedPosition = lastRemovedPosition;
    }

    public void recoverLastRemovedShot(AnimationAdapter mAnimationAdapter){
        mShots.add(lastRemovedPosition, lastRemovedShot);
        mAnimationAdapter.notifyItemInserted(lastRemovedPosition);
        mAnimationAdapter.notifyItemRangeChanged(lastRemovedPosition, getItemCount());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected ImageView shotImage;
        protected TextView shotTitle;

        public ViewHolder(View v) {
            super(v);
            shotImage = (ImageView) v.findViewById(R.id.shot_image);
            shotTitle = (TextView) v.findViewById(R.id.shot_title);

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openShotDetailFragment();
                }
            });

        }

        private void openShotDetailFragment(){
            Shot shot = mShots.get(getPosition());
            ((ActionBarActivity)mContext).getFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(
                            R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                            R.animator.card_flip_left_in, R.animator.card_flip_left_out)
                    .replace(R.id.content_frame, ShotDetailFragment.newInstance(shot, shot.player))
                    .addToBackStack(null)
                    .commit();

        }
    }
}
