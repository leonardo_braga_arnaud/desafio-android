package br.eti.arnaud.desafioandroid;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;


import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.eti.arnaud.desafioandroid.classes.InternalStorage;
import br.eti.arnaud.desafioandroid.classes.LeftDrawerAdapter;
import br.eti.arnaud.desafioandroid.classes.OnFinishPageDownload;
import br.eti.arnaud.desafioandroid.classes.RemoteDataCache;
import br.eti.arnaud.desafioandroid.classes.UndoBarController;
import br.eti.arnaud.desafioandroid.fragments.BonusFragment;
import br.eti.arnaud.desafioandroid.fragments.HomeFragment;
import br.eti.arnaud.desafioandroid.models.dribbble.Page;
import br.eti.arnaud.desafioandroid.models.LeftMenuItem;
import br.eti.arnaud.desafioandroid.models.dribbble.Shot;


public class MainActivity extends ActionBarActivity implements
          RemoteDataCache
        , BonusFragment.OnFragmentInteractionListener {

    private static final String TAG = "DESAFIO_ANDROID";
    private static final String DRIBBBLE_PAGES = "DRIBBBLE_PAGES";

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;

    private final ArrayList<LeftMenuItem> leftMenuItems = new ArrayList<LeftMenuItem>(){{
        add(new LeftMenuItem(R.string.home, R.mipmap.ic_home_white_36dp));
        add(new LeftMenuItem(R.string.bonus, R.mipmap.ic_redeem_white_36dp));
    }};

    private ArrayList<Page> mDribbblePages = new ArrayList<>();
    private ArrayList<Shot> mRemoveStack = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadDataFromCache();
        configToolbarAndDrawerToogle();
        configLeftDrawerOptions();
        openFragment(HomeFragment.newInstance());

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.clear_cache) {
            clearCache();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /*
     * Configura a toolbar e o menu à esquerda.
     */
    private void configToolbarAndDrawerToogle(){
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout
                , mToolbar, R.string.open, R.string.close) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);

            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);

                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        ActionBar mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mActionBar.setHomeButtonEnabled(true);


        mDrawerToggle.syncState();
    }

    /*
     * Atribui ações aos itens do menu à esquerda.
     */
    private void configLeftDrawerOptions(){
        ListView mleftDrawer = (ListView) findViewById(R.id.left_drawer);
        LeftDrawerAdapter mLeftDrawerAdapter = new LeftDrawerAdapter(this, leftMenuItems);
        mleftDrawer.setAdapter(mLeftDrawerAdapter);
        mleftDrawer.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int menuItemId = leftMenuItems.get(position).titleRes;

                switch (menuItemId) {

                    case R.string.home:
                        openFragment(HomeFragment.newInstance());
                        break;

                    case R.string.bonus:
                        openFragment(BonusFragment.newInstance());
                        break;

                   default:
                        Log.i(TAG, "Ocorreu um erro no LeftDrawer");
                        break;
                }
                mDrawerLayout.closeDrawers();
            }
        });
    }

    private void openFragment(Fragment fragment){
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.content_frame, fragment)
                .addToBackStack(null)
                .commit();
    }

    private void loadDataFromCache(){
        try {
            mDribbblePages = (ArrayList<Page>) InternalStorage.readObject(this, DRIBBBLE_PAGES);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void writeDataToCache(){
        try {
            InternalStorage.writeObject(this, DRIBBBLE_PAGES, mDribbblePages);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void clearCache(){
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        try {
            InternalStorage.writeObject(this, DRIBBBLE_PAGES, new ArrayList<Page>());
        } catch (IOException e) {
            e.printStackTrace();
        }
        mDribbblePages = new ArrayList<>();
        openFragment(HomeFragment.newInstance());
    }

    private void findAndRemoveShotsFromPages(){
        for (Shot shot : mRemoveStack){
            for (Page page : mDribbblePages){
                if (page.shots.remove(shot)){
                    break;
                }
            }
        }
    }

    public boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        // test for connection
        if (cm.getActiveNetworkInfo() != null
                && cm.getActiveNetworkInfo().isAvailable()
                && cm.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            //no conection
            return false;
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public ArrayList<Page> getDribbblePages() {
        return mDribbblePages;
    }

    @Override
    public void downloadPage(int pageNumber, final OnFinishPageDownload onFinishPageDownload) {
        Ion.with(this)
                .load(Page.getPageUrl(pageNumber))
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        Page page = new Gson().fromJson(result, Page.class);
                        mDribbblePages.add(page);
                        onFinishPageDownload.onFinish(mDribbblePages);
                    }
                });
    }

    @Override
    public void addToRemoveStack(Shot shot) {
        mRemoveStack.add(shot);
    }

    @Override
    public void removeLastFromRemoveStack() {
        int size = mRemoveStack.size();
        if (size > 0){
            mRemoveStack.remove(mRemoveStack.size() - 1);
        }

    }

    @Override
    public void onBackPressed(){
        if (getFragmentManager().getBackStackEntryCount() > 1){
            getFragmentManager().popBackStack();

        } else {
            super.onBackPressed();
        }

    }

    @Override
    protected void onDestroy() {
        findAndRemoveShotsFromPages();
        writeDataToCache();
        super.onDestroy();

    }
}
