package br.eti.arnaud.desafioandroid.models.dribbble;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Leonardo on 22/03/2015.
 */
public class Player implements Parcelable, Serializable{
    public long     id;
    public String   name;
    public String   location;
    public long     followers_count;
    public long     draftees_count;
    public long     likes_count;
    public long     likes_received_count;
    public long     comments_count;
    public long     comments_received_count;
    public long     rebounds_count;
    public long     rebounds_received_count;
    public String   url;
    public String   avatar_url;
    public String   username;
    public String   twitter_screen_name;
    public String   website_url;
    public long     drafted_by_player_id;
    public long     shots_count;
    public long     following_count;
    public String   created_at;

    public Player(){}

    private Player(Parcel in) {
        id = in.readLong();
        name = in.readString();
        location = in.readString();
        followers_count = in.readLong();
        draftees_count = in.readLong();
        likes_count = in.readLong();
        likes_received_count = in.readLong();
        comments_count = in.readLong();
        comments_received_count = in.readLong();
        rebounds_count = in.readLong();
        rebounds_received_count = in.readLong();
        url = in.readString();
        avatar_url = in.readString();
        username = in.readString();
        twitter_screen_name = in.readString();
        website_url = in.readString();
        drafted_by_player_id = in.readLong();
        shots_count = in.readLong();
        following_count = in.readLong();
        created_at = in.readString();
    }

    public static final Parcelable.Creator<Player> CREATOR = new Parcelable.Creator<Player>() {
        public Player createFromParcel(Parcel in) {
            return new Player(in);
        }

        public Player[] newArray(int size) {
            return new Player[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(location);
        dest.writeLong(followers_count);
        dest.writeLong(draftees_count);
        dest.writeLong(likes_count);
        dest.writeLong(likes_received_count);
        dest.writeLong(comments_count);
        dest.writeLong(comments_received_count);
        dest.writeLong(rebounds_count);
        dest.writeLong(rebounds_received_count);
        dest.writeString(url);
        dest.writeString(avatar_url);
        dest.writeString(username);
        dest.writeString(twitter_screen_name);
        dest.writeString(website_url);
        dest.writeLong(drafted_by_player_id);
        dest.writeLong(shots_count);
        dest.writeLong(following_count);
        dest.writeString(created_at);

    }

}
