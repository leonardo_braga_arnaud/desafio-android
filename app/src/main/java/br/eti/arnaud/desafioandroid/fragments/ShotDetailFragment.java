package br.eti.arnaud.desafioandroid.fragments;


import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hardsoftstudio.real.textview.views.RealTextView;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import br.eti.arnaud.desafioandroid.MainActivity;
import br.eti.arnaud.desafioandroid.R;
import br.eti.arnaud.desafioandroid.models.dribbble.Player;
import br.eti.arnaud.desafioandroid.models.dribbble.Shot;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ShotDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ShotDetailFragment extends Fragment {

    private static final String SHOT = "SHOT";
    private static final String PLAYER = "PLAYER";

    private Shot mShot;
    private Player mPlayer;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param shot Parceable da classe Shot.
     * @return A new instance of fragment ShotDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ShotDetailFragment newInstance(Shot shot, Player player) {
        ShotDetailFragment fragment = new ShotDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(SHOT, shot);
        args.putParcelable(PLAYER, player);
        fragment.setArguments(args);
        return fragment;
    }

    public ShotDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mShot = getArguments().getParcelable(SHOT);
            mPlayer = getArguments().getParcelable(PLAYER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_shot_detail, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Instanciar views
        TextView mShotTitleTextView = (TextView) getView().findViewById(R.id.shot_title);
        TextView mPlayerNameTextView = (TextView) getView().findViewById(R.id.player_name);
        RealTextView mShotDescriptionTextView = (RealTextView)getView()
                .findViewById(R.id.shot_description);
        ImageView mShotImageView = (ImageView)getView().findViewById(R.id.shot_image);
        final CircleImageView avatarImageView = (CircleImageView)
                getView().findViewById(R.id.avatar_image);

        // Atibuir valores
        mShotTitleTextView.setText(mShot.title);
        mPlayerNameTextView.setText(mPlayer.name);
        if (mShot.description == null){
            mShot.description = getActivity().getResources().getString(R.string.no_description);
        }
        mShotDescriptionTextView.setHtmlFromString(mShot.description, false);

        // Baixar imagens
        Ion.with(mShotImageView)
                .placeholder(R.mipmap.no_image)
                .error(R.mipmap.no_image)
                .load(mShot.image_url);
        Ion.with(getActivity())
                .load(mPlayer.avatar_url)
                .withBitmap()
                .placeholder(R.mipmap.no_image)
                .error(R.mipmap.no_image)
                .asBitmap()
                .setCallback(new FutureCallback<Bitmap>() {
                    @Override
                    public void onCompleted(Exception e, Bitmap bitmap) {
                        avatarImageView.setImageBitmap(bitmap);
                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}
