package br.eti.arnaud.desafioandroid.models;

/**
 * Created by Leonardo on 22/03/2015.
 */
public class LeftMenuItem {

    public LeftMenuItem(int titleRes, int imageRes){
        this.titleRes = titleRes;
        this.imageRes = imageRes;
    }

    public int titleRes;
    public int imageRes;


}
