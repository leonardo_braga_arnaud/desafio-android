package br.eti.arnaud.desafioandroid.classes;

import java.util.ArrayList;

import br.eti.arnaud.desafioandroid.models.dribbble.Page;
import br.eti.arnaud.desafioandroid.models.dribbble.Shot;

/**
 * Created by Leonardo on 22/03/2015.
 */
public interface RemoteDataCache {
    public ArrayList<Page> getDribbblePages();
    public void downloadPage(int pageNumber, OnFinishPageDownload onFinishPageDownload);
    public void addToRemoveStack(Shot shot);
    public void removeLastFromRemoveStack();

}
