package br.eti.arnaud.desafioandroid.models.dribbble;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Leonardo on 22/03/2015.
 */
public class Page implements Serializable {

    private static final String PAGE_URL = "http://api.dribbble.com/shots/popular?page=";

    public String page;
    public int per_page;
    public int pages;
    public long total;
    public List<Shot> shots;

    public static String getPageUrl(int pageNumber){
        return PAGE_URL + String.valueOf(pageNumber);
    }
}
