package br.eti.arnaud.desafioandroid.models.dribbble;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by Leonardo on 22/03/2015.
 */
public class Shot implements Parcelable, Serializable {
    public long id;
    public String title;
    public String description;
    public int height;
    public int width;
    public int likes_count;
    public int comments_count;
    public int rebounds_count;
    public String url;
    public String short_url;
    public long views_count;
    public long rebound_source_id;
    public String image_url;
    public String image_teaser_url;
    public String image_400_url;
    public String created_at;
    public Player player;

    public Shot(){}

    private Shot(Parcel in) {
        id = in.readLong();
        title = in.readString();
        description = in.readString();
        height = in.readInt();
        width = in.readInt();
        likes_count = in.readInt();
        comments_count = in.readInt();
        rebounds_count = in.readInt();
        url = in.readString();
        short_url = in.readString();
        views_count = in.readLong();
        rebound_source_id = in.readLong();
        image_url = in.readString();
        image_teaser_url = in.readString();
        image_400_url = in.readString();
        created_at = in.readString();
    }

    public static final Parcelable.Creator<Shot> CREATOR = new Parcelable.Creator<Shot>() {
        public Shot createFromParcel(Parcel in) {
            return new Shot(in);
        }

        public Shot[] newArray(int size) {
            return new Shot[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(title);
        dest.writeString(description);
        dest.writeInt(height);
        dest.writeInt(width);
        dest.writeInt(likes_count);
        dest.writeInt(comments_count);
        dest.writeInt(rebounds_count);
        dest.writeString(url);
        dest.writeString(short_url);
        dest.writeLong(views_count);
        dest.writeLong(rebound_source_id);
        dest.writeString(image_url);
        dest.writeString(image_teaser_url);
        dest.writeString(image_400_url);
        dest.writeString(created_at);

    }


}
