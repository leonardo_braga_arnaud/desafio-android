package br.eti.arnaud.desafioandroid.classes;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import br.eti.arnaud.desafioandroid.R;
import br.eti.arnaud.desafioandroid.models.LeftMenuItem;

/**
 * Created by Leonardo on 22/03/2015.
 */
public class LeftDrawerAdapter extends ArrayAdapter<LeftMenuItem> {
    private LayoutInflater mInflater;
    private final int layoutId = R.layout.left_drawer_item;

    static class ViewHolder {
        public TextView textView;
        public ImageView imageView;
    }

    public LeftDrawerAdapter(Activity context, ArrayList<LeftMenuItem> leftMenuItemlist) {
        super(context, R.layout.left_drawer_item, leftMenuItemlist);
        mInflater = LayoutInflater.from(getContext());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.left_drawer_item, parent, false);
            holder = new ViewHolder();
            holder.textView = (TextView) convertView.findViewById(R.id.textView);
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageView);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        LeftMenuItem leftMenuitem = getItem(position);
        holder.textView.setText(getContext().getString(leftMenuitem.titleRes));
        holder.imageView.setImageResource(leftMenuitem.imageRes);

        return convertView;
    }
}
